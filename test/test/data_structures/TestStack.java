package test.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Stack;

public class TestStack
{
	private Stack stack;

	@Before
	public void setup()
	{
		stack = new Stack<String>();
	} 


	@Test 
	public void Stack()
	{
		assertTrue(stack!=null);
		assertEquals(0, stack.size());
	}

	@Test
	public void testPush()
	{
		for (int i = 0; i < 508; i++) 
		{
			stack.push("Hola" + i);
		}
		assertTrue(!stack.isEmpty());
		assertEquals(508, stack.size());
		assertEquals("Hola507", stack.tope());

	}

	@Test
	public void testPop()
	{
		for (int i = 506; i > 0; i--) 
		{
			stack.push(i);
			stack.pop();
		}
		assertTrue(stack.isEmpty());
	}
	
	@Test
	public void testPop2()
	{
		for (int i = 0; i < 508; i++) 
		{
			stack.push("Hola" + i);
		}
		stack.pop();
		assertTrue(!stack.isEmpty());
		assertEquals(507, stack.size());
		assertEquals("Hola506", stack.tope());
	}

}
