package test.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;

public class TestQueue
{
	private Queue<String> q;

	@Before
	public void setup()
	{
		q = new Queue<String>();
	} 


	@Test 
	public void Queue()
	{
		assertTrue(q!=null);
		assertEquals(0, q.size());
	}

	@Test
	public void testEnqueue()
	{

		q.enqueue("Hola" + 250);
		assertTrue(!q.isEmpty());
		assertEquals(1, q.size());
		assertEquals("Hola250", q.first());
		
		q.enqueue("Hola" + 2);
		assertEquals(2, q.size());
		assertEquals("Hola250", q.dequeue());
		assertEquals("Hola2", q.first());
	}

	@Test
	public void testDequeue()
	{
		for (int i = 0; i < 508; i++) 
		{
			q.enqueue("Hola" + i);
		}

		for (int i = 0; i < 200; i++) 
		{
			q.dequeue();
		}

		assertTrue(!q.isEmpty());
		assertEquals(308, q.size());
		assertEquals("Hola200", q.first());
		assertEquals("Hola200", q.dequeue());
		
		for (int i = 0; i < 306; i++) 
		{
			q.dequeue();
		}
		
		assertTrue(!q.isEmpty());
		assertEquals("Hola507", q.dequeue());
		assertTrue(q.isEmpty());
	}
}
