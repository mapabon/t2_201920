package model.data_structures;

import java.util.Iterator;

public class LinkedList <T> implements Lista <T>, Iterable<T>
{

	private Node<T> first;
	
	private Node<T> last;

	private int listSize;
	
	


	public LinkedList ()
	{
		first = null;
		listSize = 0;
	}


	public LinkedList (T item)
	{
		first = new Node<T> (item) ;
		listSize = 1;
	}

	@Override
	public int size()
	{
		return listSize;
	}

	@Override
	public boolean isEmpty()
	{
		boolean respuesta = true;
		if(listSize > 0)
		{
			respuesta = false;
		}
		return respuesta;
	}

	@Override
	public T get (int pos) 
	{
		if(pos < listSize)
		{
			Node<T> n = first;
			int i = 0;
			
			while(i < pos && n != null)
			{
				n = n.getNext();
				i++;
			}
			
			return n.getItem();
		}
		else
		{
			return null;
		}
	}
	

	@Override
	public void addFirst (T item)
	{
		Node <T> newHead = new Node<> (item);
		newHead.setNextNode(first);
		first = newHead;
		listSize++;
	}

	@Override
	public void append(T item) 
	{
		Node <T> newNode = new Node<> (item);

		if(first == null) 
		{
			first = newNode;
			last = newNode;
		}

		else 
		{
			last.setNextNode(newNode);
			last = last.getNext();
		}

		listSize++;
	}

	@Override
	public T removeFirst()
	{	
		T rta = first.getItem();
		if(listSize > 0)
		{
			Node<T> a = first;
			first = first.getNext();
			a.setNextNode(null);
			listSize--;
		}
		else
		{
			first = null;
		}
		return rta;
	}

	@Override
	public void remove (int pos)
	{
		if(pos == 0)
		{
			Node<T> a = first;
			first = first.getNext();
			a.setNextNode(null);
			listSize --;
		}
		
		else if(pos < listSize)
		{
			Node<T> n = first;
			int i = 0;
			
			while(i < pos-1 && n != null)
			{
				n = n.getNext();
				i++;
			}
			
			if(n.getNext() != null)
			{
				Node<T> a = n.getNext();
				n.setNextNode(n.getNext().getNext());
				a.setNextNode(null);
				listSize --;
			}
			else 
			{
				n.setNextNode(null);
				last = n;
				listSize --;
			}
		}
	}

	
	
	
	@Override
	public Iterator<T> iterator()
	{
		return new Iterator <T> ()
		{
			
			Node <T> act = null;
			
			public boolean hasNext()
			{
				if (listSize == 0)
				return false;
				if (act == null)
					return true;
				return act.getNext() != null;
			}
			
			public T next()
			{
				if (act == null)
					act = first;
				else
					act = act.getNext();
				return act.getItem();
			}
			
		};
	}


	public Node<T> getLast() {
		return last;
	}


	public void setLast(Node<T> last) {
		this.last = last;
	}


}
