package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T>
{
	private int size;
	private Node<T> first;


	public Stack()
	{
		size = 0;
		first = null;
	}

	@Override
	public void push(T elem) 
	{
		Node<T> nuevo = new Node<T> (elem);
		if (first == null)
		{
			first =nuevo;
			size ++;
		}
		else
		{
			first.setBefore(nuevo);
			nuevo.setNextNode(first);
			first = nuevo;
			size ++;
		}
	}

	@Override
	public T pop()
	{
		if(first == null)
		{
			return null;
		}
		else if (first.getNext()!= null)
		{
			Node<T> out = first;
			first.getNext().setBefore(null);
			first = first.getNext();
			out.setNextNode(null);
			size--;
			return out.getItem();
		}
		else
		{
			Node<T> out = first;
			first = null;
			size--;
			return out.getItem();
		}
	}

	@Override
	public int size()
	{
		return size;
	}

	public boolean isEmpty()
	{
		return (size == 0);
	}

	@Override
	public T tope()
	{
		T rta = first.getItem();
		return rta;
	}
	
	public Iterator<T> iterator()
	{
		return new Iterator <T> ()
		{

			Node <T> act = null;

			public boolean hasNext()
			{
				if (size == 0)
					return false;
				if (act == null)
					return true;
				return act.getNext() != null;
			}

			public T next()
			{
				if (act == null)
					act = first;
				else
					act = act.getNext();
				return act.getItem();
			}

		};
	}

}
