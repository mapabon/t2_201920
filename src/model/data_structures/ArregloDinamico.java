package model.data_structures;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico<T extends Comparable<T>> implements IArregloDinamico<T> {
	/**
	 * Capacidad maxima del arreglo
	 */
    private int tamanoMax;
	/**
	 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
	 */
    private int tamanoAct;
    /**
     * Arreglo de elementos de tamaNo maximo
     */
    private T[ ] elementos;

    /**
     * Construir un arreglo con la capacidad maxima inicial.
     * @param max Capacidad maxima inicial
     */
	public ArregloDinamico( int max )
    {
           elementos =  (T []) new Object[max];
           tamanoMax = max;
           tamanoAct = 0;
    }
    
	public void agregar( T dato )
    {
           if ( tamanoAct == tamanoMax )
           {  // caso de arreglo lleno (aumentar tamaNo)
                tamanoMax = 2 * tamanoMax;
                T [ ] copia = elementos;
                elementos = (T [ ]) new Object[tamanoMax];
                for ( int i = 0; i < tamanoAct; i++)
                {
                 	 elementos[i] = copia[i];
                } 
        	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
           }	
           elementos[tamanoAct] = dato;
           tamanoAct++;
   }

	public int darTamano() 
	{
		return tamanoAct;
	}
	
	public int darCapacidad() {
		return tamanoMax;
	}

	public T darElemento(int i) 
	{
		return elementos[i];
	}

	public T buscar(T dato) 
	{
		T a = null;
		
		for(int i = 0; i < tamanoAct; i++)
		{
			if(elementos[i].toString().compareTo(dato.toString()) == 0)
			{
				a = elementos[i];
			}
		}
		return a;
	}

	public T eliminar(T dato) 
	{
		T temp = null;
		
		for(int i = 0; i < tamanoAct; i++)	
		{
			if(elementos[i].toString().compareTo(dato.toString()) == 0)
			{
				temp = elementos[i];
				for (int j = i; j < tamanoAct - 1; j++) 
				{
                    elementos[j] = elementos[j+1];
				}
				tamanoAct = tamanoAct - 1;
			}
		}
		return temp;
	}
}