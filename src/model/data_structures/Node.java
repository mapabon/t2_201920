package model.data_structures;

public class Node <E>
{


	private Node<E> next;
	private Node<E> before;


	private E item;


	public Node (E item) 
	{
		next = null;
		this.item = item;
		before = null;
	}


	public Node<E> getNext() 
	{
		return next;
	}


	public void setNextNode ( Node<E> next) 
	{
		this.next = next;
	}

	public Node<E> getBefore() {
		return before;
	}


	public void setBefore(Node<E> before) 
	{
		this.before = before;
	}
	
	public E getItem()
	{
		return item;
	}


	public void setItem (E item) 
	{
		this.item = item;
	}
}

