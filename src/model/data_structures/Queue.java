package model.data_structures;


public class Queue<T> implements IQueue<T>
{

	//------------------------------------------------------------------------------
	// Attributes
	//------------------------------------------------------------------------------

	private Node<T> first;

	private Node<T> last;

	private int queueSize;

	//------------------------------------------------------------------------------
	// Constructors
	//------------------------------------------------------------------------------

	public Queue()
	{
		first = null;
		last = null;
		queueSize = 0;
	}

	public Queue(T item)
	{
		first = new Node<T> (item);
		last = new Node<T> (item);
		queueSize = 1;
	}

	//------------------------------------------------------------------------------
	// Methods
	//------------------------------------------------------------------------------

	@Override
	public void enqueue(T item) 
	{
		Node<T> nuevo = new Node<T>(item);

		if(queueSize == 0)
		{
			first = nuevo;
			last = nuevo;
			queueSize++;
		}
		else
		{
			last.setNextNode(nuevo);
			last = last.getNext();
			queueSize++;
		}
	}

	@Override
	public T dequeue() 
	{
		if(first != null)
		{
			Node<T> out = first;
			first = first.getNext();
			out.setNextNode(null);
			queueSize--;
			return out.getItem();
		}
		else
		{
			return null;
		}
	}

	@Override
	public T first() 
	{
		if(first != null)
			return first.getItem();
		else 
			return null;
	}

	@Override
	public int size() 
	{
		return queueSize;
	}

	@Override
	public boolean isEmpty() 
	{
		if(queueSize > 0)
			return false;
		else
			return true;
	}

}
