package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.Queue;
import model.data_structures.Stack;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{

	//----------------------------------------------------------------------------------------------------------------------------------
	// Attributes
	//----------------------------------------------------------------------------------------------------------------------------------

	private Stack<Viaje> stack;
	private Queue<Viaje> queue;
	private int cont;
	private int hora;


	//----------------------------------------------------------------------------------------------------------------------------------
	// Constructors
	//----------------------------------------------------------------------------------------------------------------------------------

	public MVCModelo()
	{
		stack = new Stack<Viaje>();
		queue = new Queue<Viaje>();
		cont = 0;
	}


	//----------------------------------------------------------------------------------------------------------------------------------
	// Queue Methods
	//----------------------------------------------------------------------------------------------------------------------------------

	public Queue<Viaje> darQueue()
	{
		return queue;
	}

	public void enqueue(Viaje dato)
	{	
		queue.enqueue(dato);
	}

	public Viaje dequeue()
	{
		return queue.dequeue();
	}

	public Viaje getFirst()
	{
		return queue.first();
	}

	public boolean isQueueempty()
	{
		return queue.isEmpty();
	}

	public int queueSize()
	{
		return queue.size();
	}


	//----------------------------------------------------------------------------------------------------------------------------------
	// Stack Methods
	//----------------------------------------------------------------------------------------------------------------------------------

	public Stack<Viaje> darStack()
	{
		return stack;
	}

	public void push(Viaje dato)
	{	
		stack.push(dato);
	}

	public Viaje pop()
	{	
		return stack.pop();
	}

	public Viaje getTop()
	{
		return stack.tope();
	}

	public boolean isStackempty()
	{
		return stack.isEmpty();
	}

	public int stackSize()
	{
		return stack.size();
	}


	//----------------------------------------------------------------------------------------------------------------------------------
	// General Methods
	//----------------------------------------------------------------------------------------------------------------------------------

	public int darContador()
	{
		return cont;
	}

	public void cargarArchivos() 
	{
		CSVReader reader = null;

		try 
		{
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));

			for(String[] nextLine : reader) 
			{
				if(nextLine[0].contains("source")){}
				else
				{
					Viaje v = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), 
							Double.parseDouble(nextLine[3]));
					enqueue(v);
					push(v);
					cont++;
				}
			}
		} 
		catch (FileNotFoundException e) {e.printStackTrace();} 

		finally
		{
			if (reader != null) 
			{
				try {reader.close();} 
				catch (IOException e) {e.printStackTrace();}
			}
		}
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		this.hora = hora;
	}




	public Queue<Viaje> clusterMax(int hStart)
	{
		Queue<Viaje> cluster = new Queue<Viaje>();
		Viaje last = null;

		while(getFirst().getHour() < hStart)
		{
			dequeue();
		}

		last = getFirst();

		while(!queue.isEmpty())
		{
			Queue<Viaje> aux = new Queue<Viaje>();	
			aux.enqueue(last);
			
			
			

			while(getFirst() != null && getFirst().getHour() >= hStart && last.getHour() <= getFirst().getHour())
			{
				last = dequeue();
				aux.enqueue(last);
				
				
			}

			if(cluster.size() < aux.size())
				cluster = aux;

			last = dequeue();
		}
		return cluster;
	}










	public Queue<Viaje> ultimosNViajes(int viajes, int hora)
	{
		Queue<Viaje> rta = new Queue<Viaje>();
		Stack<Viaje> nuevo = new Stack<Viaje>();
		Iterator<Viaje> itr = stack.iterator();
		while(itr.hasNext())
		{
			nuevo.push(itr.next());
		}
		while (nuevo.size()!=0 && rta.size()<viajes)
		{
			Viaje top = nuevo.pop();
			if(top.getHour()== hora)
			{
				rta.enqueue(top);
			}
		}
		return rta;
	}



}
