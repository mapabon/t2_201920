package model.logic;

public class Viaje 
{
	//----------------------------------------------------------------------------------------------------------------------------------
	// Attributes
	//----------------------------------------------------------------------------------------------------------------------------------

	private int sourceid;
	private int dstid;
	private int month;
	private int hour;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;


	//----------------------------------------------------------------------------------------------------------------------------------
	// Constructors
	//----------------------------------------------------------------------------------------------------------------------------------

	//General Constructor (All attributes)
	public Viaje(int pSource, int pDstid, int pMonth, int pHour, double pMean_travel_time, double pStandard_deviation_travel_time, 
			double pGeometric_mean_travel_time, double pGeometric_standard_deviation_travel_time)
	{
		setSourceid(pSource);
		setDstid(pDstid);
		setMonth(pMonth);
		setHour(pHour);
		setMean_travel_time(pMean_travel_time);
		setStandard_deviation_travel_time(pStandard_deviation_travel_time);
		setGeometric_mean_travel_time(pGeometric_mean_travel_time);
		setGeometric_standard_deviation_travel_time(pGeometric_standard_deviation_travel_time);
	}


	//Constructor for Taller 2
	public Viaje(int pSource, int pDstid, int pHour, double pMean_travel_time)
	{
		setSourceid(pSource);
		setDstid(pDstid);
		setHour(pHour);
		setMean_travel_time(pMean_travel_time);
		setMonth(0);
		setStandard_deviation_travel_time(0);
		setGeometric_mean_travel_time(0);
		setGeometric_standard_deviation_travel_time(0);
	}

	//Constructor for Taller 1
	public Viaje(int pSource, int pDstid, int pMonth, double pMean_travel_time, double pStandard_deviation_travel_time, 
			double pGeometric_mean_travel_time, double pGeometric_standard_deviation_travel_time)
	{
		setSourceid(pSource);
		setDstid(pDstid);
		setMonth(pMonth);
		setMean_travel_time(pMean_travel_time);
		setStandard_deviation_travel_time(pStandard_deviation_travel_time);
		setGeometric_mean_travel_time(pGeometric_mean_travel_time);
		setGeometric_standard_deviation_travel_time(pGeometric_standard_deviation_travel_time);
	}

	//----------------------------------------------------------------------------------------------------------------------------------
	// Methods
	//----------------------------------------------------------------------------------------------------------------------------------

	//Source ID
	public int getSourceid() {
		return sourceid;
	}

	public void setSourceid(int sourceid) {
		this.sourceid = sourceid;
	}

	//Dst ID
	public int getDstid() {
		return dstid;
	}

	public void setDstid(int dstid) {
		this.dstid = dstid;
	}

	//Month
	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	//Hour
	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	//Mean Travel Time
	public double getMean_travel_time() {
		return mean_travel_time;
	}

	public void setMean_travel_time(double mean_travel_time) {
		this.mean_travel_time = mean_travel_time;
	}

	//Standard Deviation Travel Time
	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}

	public void setStandard_deviation_travel_time(double standard_deviation_travel_time) {
		this.standard_deviation_travel_time = standard_deviation_travel_time;
	}

	//Geometric Mean Travel Time
	public double getGeometric_mean_travel_time() {
		return geometric_mean_travel_time;
	}

	public void setGeometric_mean_travel_time(double geometric_mean_travel_time) {
		this.geometric_mean_travel_time = geometric_mean_travel_time;
	}

	//Geometric Standard Deviation Travel Time
	public double getGeometric_standard_deviation_travel_time() {
		return geometric_standard_deviation_travel_time;
	}

	public void setGeometric_standard_deviation_travel_time(double geometric_standard_deviation_travel_time) {
		this.geometric_standard_deviation_travel_time = geometric_standard_deviation_travel_time;
	}
}
