package controller;

import java.util.Scanner;

import model.data_structures.Queue;
import model.logic.MVCModelo;
import model.logic.Viaje;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";

		while( !fin )
		{
			view.printMenu();

			int option = lector.nextInt();
			switch(option)
			{
			case 1:
				modelo = new MVCModelo();
				modelo.cargarArchivos();
				System.out.println("Archivos cargados con �xito.");
				System.out.println("Numero de viajes leidos: " + modelo.darContador() + "\n---------");	
				
				System.out.println("Datos primer viaje");
				System.out.println("Zona origen: " + modelo.getFirst().getSourceid());	
				System.out.println("Zona destino: " + modelo.getFirst().getDstid());
				System.out.println("Hora de viaje: " + modelo.getFirst().getHour());	
				System.out.println("Tiempo promedio de Viaje: " + modelo.getFirst().getMean_travel_time());	
				System.out.println("");
				System.out.println("Datos ultimo viaje");
				System.out.println("Zona origen: " + modelo.getTop().getSourceid());	
				System.out.println("Zona destino: " + modelo.getTop().getDstid());
				System.out.println("Hora de viaje: " + modelo.getTop().getHour());	
				System.out.println("Tiempo promedio de Viaje: " + modelo.getTop().getMean_travel_time());
				System.out.println("");
				break;

				
			case 2:
				System.out.println("--------- \nDar hora deseada (XX): ");
				dato = lector.next();
				modelo.setHora(Integer.parseInt(dato));
				
				Queue<Viaje> aux = modelo.clusterMax(modelo.getHora()); 
				int x = aux.size();
				
				System.out.println("Cantidad de viajes del cluster m�s grande: " + x);
				
				for(int i = 0; i < x; i++)
				{
					Viaje v = aux.dequeue();
					
					System.out.println("Datos del viaje " + (i+1) + " :");
					System.out.println("Hora de viaje: " + v.getHour());
					System.out.println("Zona origen: " + v.getSourceid());	
					System.out.println("Zona destino: " + v.getDstid());
					System.out.println("Tiempo promedio de Viaje: " + v.getMean_travel_time());
					System.out.println();
				}
				break;

			case 3:
				System.out.println("--------- \nDar hora deseada (xx): ");
				int hora = lector.nextInt();
				
				System.out.println("--------- \nDar cantidad de los ultimos viajes que desea ver : ");
				int n = lector.nextInt();
				
				Queue<Viaje> rta = modelo.ultimosNViajes(n, hora);
				
				int y = rta.size();
				
				for(int i = 0; i < y; i++)
				{
					Viaje data = rta.dequeue();
					
					System.out.println("Datos del viaje " + (i+1) + " :");
					System.out.println("Hora de viaje: " + data.getHour());
					System.out.println("Zona origen: " + data.getSourceid());	
					System.out.println("Zona destino: " + data.getDstid());
					System.out.println("Tiempo promedio de Viaje: " + data.getMean_travel_time());
					System.out.println();
				}
				break;

			case 4: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;	

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
